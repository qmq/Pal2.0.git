import QGame from './Tools/QGame';
const {ccclass, property} = cc._decorator;

@ccclass
export default class CameraFollow extends cc.Component {
    @property(cc.Node)
    target:cc.Node=null;
    
    private limitMin:cc.Vec2;
    private limitMax:cc.Vec2;

    onLoad () {
        let mapSize = cc.find("Canvas/MapBg").getContentSize();        
        let x = (mapSize.width-cc.winSize.width)/2
        let y = (mapSize.height-cc.winSize.height)/2
        if(x<0) x = 0;
        if(y<0) y = 0;        
        this.limitMin = cc.v2(-x,-y);
        this.limitMax = cc.v2(x,y);        
    }

    start () {
        let player = QGame.Instance.Team.getAt(0);
        if(player){
            this.target =player._node;
        }
    }
    
    update (dt) {
        if(!this.target || !this.limitMin)return;
        let p1 = this.target.position;      
        p1 = p1.clampf(this.limitMin,this.limitMax);
        this.node.setPosition(p1);
    }
}
