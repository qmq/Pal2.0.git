import Db from '../Tools/Db';
import QGame from '../Tools/QGame';
import { ButtleDat, PlayerDat, ItemDat, CharacterDir } from '../Tools/QType';
import Player from '../Game/Player';



const {ccclass, property} = cc._decorator;

@ccclass
export default class StartManger extends cc.Component {
    private newBtn:cc.Node;
    private loadBtn:cc.Node;
    private exitBtn:cc.Node;
    private loadBar:cc.ProgressBar;
    onLoad(){
        let  collision = cc.director.getCollisionManager()
        collision.enabled = true;
        collision.enabledDebugDraw = true;
        cc.debug.setDisplayStats(false)
     
     
        let btns = this.node.getChildByName("btns");
        btns.active = false;
        this.newBtn = btns.getChildByName("new");
        this.loadBtn = btns.getChildByName("load");
        this.exitBtn = btns.getChildByName("exit");
        this.loadBar = this.node.getChildByName("loadBar").getComponent(cc.ProgressBar);
        this.loadBar.node.active = true;

        cc.loader.loadResDir("",(count,total,item)=>{
            this.loadBar.progress = count/total;            
        },(err,res)=>{
            this.loadBar.node.active = false;
            btns.active = true;
            this.initDb();
            this.newBtn.on(cc.Node.EventType.TOUCH_START,this.newGame,this)
            this.loadBtn.on(cc.Node.EventType.TOUCH_START,this.loadGame,this)
            this.exitBtn.on(cc.Node.EventType.TOUCH_START,this.exitGame,this)                        
        })

    }
    /**
     * 初始化数据
     */
    initDb(){
        cc.loader.loadResDir("Dat/Db",cc.TextAsset,(t,s,item)=>{           
            this.parseCsv(item.content);
        },(err,res)=>{
            //cc.log(res);
        })
    }

    /** 解析CSV */
    parseCsv(textAsset:cc.TextAsset){           
        let str = textAsset.text;
        let name = textAsset.name;        
        let row = str.trim().split("\n");        
        let fields = row.shift().split(",");
        
        /**
         * 类型
         */
        let types = {};
        for(let i=0;i<fields.length;i++){
            if(fields[i].indexOf(":")>-1){
                let ta = fields[i].split(":")
                fields[i] = ta[0];
                types[ta[0]] = ta[1];
            }
        }
        
    
        let pk = fields[0];
        let dat = {};         
        while(row.length){
            let item = row.shift().split(",");
            
            let obj = {};
            for(let i=0;i<fields.length;i++){                
                let f = fields[i].toString().trim();
                let value;
                value = isNaN(Number( item[i] ))?item[i]:Number(item[i]);
                if(types.hasOwnProperty(f)){                    
                    if(types[f]=="object"){
                        let tempObj = {};
                        if(value){
                            (value as string).split(";").forEach(item=>{
                                let tempAttr = item.split(":");
                                tempObj[tempAttr[0]] = isNaN(Number( tempAttr[1] ))?tempAttr[1]:Number(tempAttr[1]);
                            })    
                        }
                        value = tempObj;
                    }
                }                 
                obj[f] =  value;
            }
            dat[obj[pk]] = obj;
        }                    
        Db.Instance.setData(name,dat);                     
    }

    newGame(){
        //新的游戏 初始化QGame;
        
        //初始化队伍
        let player1 = Db.Instance.getItem<PlayerDat>("Character","少年王小虎");
        player1.x = 1188
        player1.y = 263
        // player1.x = 0
        // player1.y = -50        
        player1.dir = CharacterDir.Right
        QGame.Instance.Map.name = "杭州城";
        QGame.Instance.Team.add(player1)
        QGame.Instance.changeMoney(9999);

        let player2 = Db.Instance.getItem<PlayerDat>("Character","苏媚")        
        QGame.Instance.Team.add(player2)


        let player3 = Db.Instance.getItem<PlayerDat>("Character","沈欺霜")        
        QGame.Instance.Team.add(player3)

        let player4 = Db.Instance.getItem<PlayerDat>("Character","李忆如")        
        QGame.Instance.Team.add(player4)

        cc.log(QGame.Instance)
        //QGame.Instance.Map.getState().enter=1;
        cc.director.loadScene("杭州城");        
    }

    loadGame(){
        cc.log("打开存档")
    }

    exitGame(){      
        cc.game.end();        
    }

    /**
     * 测试背包数据
     */
    testInitBack(){
        //初始化背包数据
        // let backpack = [
        //     {"id":1,"count":2},
        //     {"id":2,"count":3},
        //     {"id":3,"count":1},
        //     {"id":4,"count":1},
        //     {"id":5,"count":1},
        //     {"id":6,"count":1},
        //     {"id":7,"count":1},
        //     {"id":8,"count":1},
        //     {"id":9,"count":1},
        //     {"id":10,"count":1},
        //     {"id":11,"count":1},
        //     {"id":31,"count":1},
        //     {"id":32,"count":1},
        //     {"id":33,"count":1},
        //     {"id":34,"count":1},
        //     {"id":35,"count":1},
        //     {"id":36,"count":1},
        //     {"id":37,"count":1},
        //     {"id":38,"count":1},
        //     {"id":61,"count":1},
        //     {"id":62,"count":1},
        //     {"id":63,"count":1},
        //     {"id":64,"count":1},
        //     {"id":65,"count":1},
        //     {"id":81,"count":1},
        //     {"id":82,"count":1},
        //     {"id":83,"count":1},
        //     {"id":84,"count":1},
        //     {"id":85,"count":1},
        //     {"id":86,"count":1},
        //     {"id":87,"count":1}            
        // ]
        // QGame.Instance.Backpack.init(backpack);
    }
}
