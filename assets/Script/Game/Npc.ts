import Character from './Basis/Character';
import { CharacterDat, NpcDat, CharacterState, CharacterType } from '../Tools/QType';
import ScriptManager from '../Tools/ScriptManager';
const {ccclass, property} = cc._decorator;

@ccclass
export default class Npc extends Character {

    onLoad(){
        super.onLoad();    
        this.node.setAnchorPoint(0.5,0);     
        this.sprite.spriteFrame = null;
        
        //加载相关素材
        cc.loader.loadRes("Character/"+this.dat.code,cc.SpriteAtlas,(err,atlas)=>{            
            this.init(atlas)
        })
        this.node.on(cc.Node.EventType.TOUCH_END,this.touchEnd,this);
    }

    /**
     * 初始化角色
     * 前4张为四方向图
     * 后为行走图 行走图只有2种 上右 左下
     */
    init(atlas:cc.SpriteAtlas){
      
        //静止动画图
        for(let i=0;i<4;i++){
            let clip = cc.AnimationClip.createWithSpriteFrames([atlas.getSpriteFrame("1"+i+"1")],1);
            clip.wrapMode = cc.WrapMode.Default;
            clip.name = CharacterState.Idle+"_"+i;            
            this.animation.addClip(clip);    
        }
        //移动动画图
        for(let i=2;i<=3;i++){
            let tempSps = [
                atlas.getSpriteFrame("2"+i+"1"),
                atlas.getSpriteFrame("2"+i+"2"),
                atlas.getSpriteFrame("2"+i+"3"),
                atlas.getSpriteFrame("2"+i+"4"),
            ];
            
            let clip = cc.AnimationClip.createWithSpriteFrames(tempSps as [cc.SpriteFrame],4);
            clip.wrapMode = cc.WrapMode.Loop;
            clip.name = CharacterState.Move+"_"+i;            
            this.animation.addClip(clip);   
           
            let clip1 = cc.AnimationClip.createWithSpriteFrames(tempSps as [cc.SpriteFrame],4);
            clip1.wrapMode = cc.WrapMode.Loop;
            let tempN;
            if(i==2){
                tempN = "1"
            }else{
                tempN = "0"
            }
            clip1.name = CharacterState.Move+"_"+tempN;
            this.animation.addClip(clip1); 
        }

        //以上两个基础动作共12张图。
        // let length = atlas.getSpriteFrames().length
        // if(length>12)

        //如果超过12张图，即有附加动作 附加动作的序号重3开始 方向为2,3
        let aIndex=3;
        while(atlas.getSpriteFrame(aIndex+"21")){
            [2,3].forEach(item=>{ 
                let kIndex=1;
                let tempSps = [];
                let tempSp;           
                while ( tempSp = atlas.getSpriteFrame(aIndex+item.toString()+kIndex)) {
                    tempSps.push(tempSp);
                    kIndex++;
                }
                
                let clip = cc.AnimationClip.createWithSpriteFrames(tempSps as [cc.SpriteFrame],tempSps.length);
                clip.wrapMode = cc.WrapMode.Loop;
                clip.name = aIndex+"_"+item;
                this.animation.addClip(clip)        
                let clip1 = cc.AnimationClip.createWithSpriteFrames(tempSps as [cc.SpriteFrame],tempSps.length);
                clip1.wrapMode = cc.WrapMode.Loop;
                clip1.name = aIndex+"_"+(item==2?1:0);
                this.animation.addClip(clip1)               
            })
            aIndex++;
        }      
        this.playAnim()                 
    }

    /**
     * Npc被点击
     */
    touchEnd(){
        //如果距离玩家队伍太远，则玩家自动移动. 否则显示对话             

        let speak = (this.dat as NpcDat).speak;
        if(!speak)return;
        let chat = speak.normal;


        //给Npc赋值商店商品
        // if(this.dat.hasOwnProperty("goods")&&chat.afterEvent&&chat.afterEvent.obj.name=="openShop"){
        //     chat.afterEvent.obj.data = (this.dat as NpcDat).goods;
        // }

        
        
        let event = new cc.Event.EventCustom("EventForWard",true);
        let obj = chat
        event.setUserData(obj)
        
        this.node.dispatchEvent(event);

        //判断是否存在条件对话
        // if(speak.hasOwnProperty("if")){
        //     let ifObj = speak.if;            
        // }else{
        //     //调用默认说话
        // }    
    }
}
