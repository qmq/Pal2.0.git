import { PlayerDat, CharacterType, PlotDat, PlotType, DialogDat, PlotCharacterMove, MapDat } from '../Tools/QType';
import Player from './Player';
import ScriptManager from '../Tools/ScriptManager';
import QGame from '../Tools/QGame';
import Npc from './Npc';
import UIManager from '../UI/UIManager';
import Screen from './Screen/Screen';

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameManager extends cc.Component {
    private mapDat:MapDat;
   
    private UIManager:UIManager;

    private Screen:Screen;


    onLoad () {
        this.UIManager = this.node.getChildByName("UI").getComponent(UIManager); 
        this.Screen = this.getComponent(Screen);
    }
    start () {
        /**
         * 加载数据
         * 
         */
        let mapName = cc.director.getScene().name
        let mapDat:cc.JsonAsset = cc.loader.getRes("Dat/Map/"+mapName,cc.JsonAsset);
           

        //获取地图数据
        if(mapDat){
            let dat = mapDat.json
            this.mapDat = dat; 
            cc.log(dat)  
            if(dat.Plot){
                this.Screen.firstEnterDat = dat.Plot
            }            
        }
     
        this.initPlayer();        
        this.initNpc();
    
      
    
    }
    /** 初始化玩家 */
    initPlayer(){
        let Team = QGame.Instance.Team;
        let prev:Player;
        for(let i=0;i<Team.length;i++){            
            let node = new cc.Node();
            let player = node.addComponent(Player);
            player.setDat(Team.getAt(i));
            node.setParent( ScriptManager.Instance.MapTerrain.node);                                               
            if(i===0){   
                player.isLeader=true;             
            }else{
                prev.pensioner = player;
            }
            prev = player;
        }   
    }

    /** 初始化Npc */
    initNpc(){
        if(!this.mapDat)return;
        let npcs = this.mapDat.npc;
        if(!npcs || !npcs.length)return;
        for(let i=0;i<npcs.length;i++){
            let node = new cc.Node();
            let npc = node.addComponent(Npc);
            npc.setDat(npcs[i]);
            node.setParent(ScriptManager.Instance.MapTerrain.node);
        }
    }
}
