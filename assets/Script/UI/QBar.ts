const {ccclass, property} = cc._decorator;

@ccclass
export default class QBar extends cc.Component {
    private progressBar:cc.ProgressBar;
    onLoad () {
        this.progressBar = this.getComponent(cc.ProgressBar);
        this.progressBar.totalLength = this.progressBar.barSprite.node.width;
    }
}
