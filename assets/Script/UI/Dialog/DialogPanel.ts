
import ScriptManager from '../../Tools/ScriptManager';
import Dialog from './Dialog';
import { DialogDat, DialogType } from '../../Tools/QType';
const {ccclass, property} = cc._decorator;

@ccclass
export default class DialogPanel extends cc.Component {
    private ChatPanel:Dialog;
    private CityPanel:Dialog;

   
    onLoad () {
        if(ScriptManager.Instance.DialogPanel) return;
        ScriptManager.Instance.DialogPanel = this;

        this.ChatPanel = this.node.getChildByName("ChatPanel").getComponent(Dialog)
        this.CityPanel = this.node.getChildByName("CityPanel").getComponent(Dialog)

    }

    show(dat:DialogDat){      
        switch(dat.type as any){
            case DialogType.Chat: 
            case DialogType[DialogType.Chat]:               
                this.ChatPanel.show(dat)
            break;
            case DialogType.City:
            case DialogType[DialogType.City]:
                this.CityPanel.show(dat)
            break;
        }
    }    


    showChat(){

    }



    end(){
      
    }
}
