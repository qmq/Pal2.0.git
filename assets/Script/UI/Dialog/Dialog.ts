import { DialogDat, NpcDat } from '../../Tools/QType';
const {ccclass, property} = cc._decorator;

@ccclass
export default class Dialog extends cc.Component {
    protected dat:DialogDat;
 

    

    onLoad(){          
        //this.node.active = false      
    }

    show(dat:DialogDat){
        this.node.active = true;
        this.dat = dat;
    }

    end(){   
        this.node.active = false;     
        if(this.dat.afterEvent){
            let after = this.dat.afterEvent;
            let e = new cc.Event.EventCustom(after.event,true);
            e.setUserData(after.obj);
            this.node.dispatchEvent(e);
        }

        let de = new cc.Event.EventCustom("DialogEnd",true);
        this.node.dispatchEvent(de);

    }



}
