import Dialog from './Dialog';
import { DialogDat } from '../../Tools/QType';
const {ccclass, property} = cc._decorator;

@ccclass
export default class ChatPanel extends Dialog {
    private nameLabel:cc.Label;
    private contentLabel:cc.Label
    private layout:cc.Layout
    /** 逐字显示中 true显示进行中  false显示已完成  */
    private writeing:boolean=false;
    /** 是否在执行中
     * 如果在执行中，无法再次加入新的对话
     */
    private isRun:boolean=false;

    onLoad(){
        super.onLoad();
        this.nameLabel = this.node.getChildByName("name").getComponent(cc.Label);
        this.contentLabel = this.node.getChildByName("content").getComponent(cc.Label);
        this.layout = this.getComponent(cc.Layout);

        this.node.on(cc.Node.EventType.TOUCH_START,this.touchStart,this);

    }
    show(dat:DialogDat){
        if(this.isRun)return;
        this.isRun = true;
        super.show(dat);

        
        this.layout.enabled = true;
        this.node.opacity=0
        if(this.dat.name){
            this.nameLabel.enabled = true;
            this.nameLabel.string = this.dat.name+":"
        }else{
            this.nameLabel.enabled = false;
        }
        
        this.contentLabel.string = this.dat.content;
        
       
        
        let l = this.dat.content.length;
        let index=0;
        this.writeing=true;
        this.schedule(()=>{
            this.layout.enabled=false
            this.node.opacity=255
            index++
            this.contentLabel.string = this.dat.content.slice(0,index)
            if(index>l){ 
                this.writeing = false;                           
            }
        },0.05,l)                                
    }

    touchStart(){
        if(this.writeing){
            this.unscheduleAllCallbacks();
            this.contentLabel.string = this.dat.content;
            this.writeing = false;                
        }else{
            this.hide()
        }
    }

    hide(){        
        let as = [            
            cc.fadeOut(1),
            cc.callFunc(()=>{
                this.isRun = false;                
                this.end()
            })
        ]
        let seq = cc.sequence(as)
        this.node.runAction(seq)
    }

}
