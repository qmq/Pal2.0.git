import Dialog from './Dialog';
import { DialogDat } from '../../Tools/QType';
const {ccclass, property} = cc._decorator;

@ccclass
export default class CityPanel extends Dialog {
    
    private contentLabel:cc.Label;

    onLoad(){
        super.onLoad();
        this.contentLabel = this.node.getChildByName("content").getComponent(cc.Label);
    }

 
    
    show(dat:DialogDat){
        super.show(dat);        
        this.node.opacity = 0;
        this.contentLabel.string = this.dat.content
        let a1 = cc.fadeIn(2);
        let a2 = cc.delayTime(1);
        let a3 = cc.fadeOut(1);        
        let a4 = cc.callFunc(()=>{
            this.node.active = false;
            this.end()
        })
        let seq = cc.sequence(a1,a2,a3,a4)
        this.node.runAction(seq)        
    }
}
