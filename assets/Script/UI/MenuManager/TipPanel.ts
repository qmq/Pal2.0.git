import { ItemDat, LanguageAttr } from '../../Tools/QType';
const {ccclass, property} = cc._decorator;

@ccclass
export default class TipPanel extends cc.Component {
    private _itemDat:ItemDat;
    private attrsParent:cc.Node;
    set itemDat(value){
        if(this.itemDat!=value){
            this._itemDat = value;
        }        
        this.updateUI();
    }
    get itemDat(){
        return this._itemDat;
    }

    private nameLabel:cc.Label;
    private desc:cc.Label;    
    onLoad(){
        this.nameLabel = this.node.getChildByName("name").getComponent(cc.Label);
        this.desc = this.node.getChildByName("desc").getComponent(cc.Label);
        this.attrsParent = this.node.getChildByName("attrs");                 
        this.node.on(cc.Node.EventType.TOUCH_START,()=>{
            
        })      
        this.node.active = false
    }


    updateUI(){
        
        if(!this.itemDat){
            this.node.active = false;
            return;
        }
        
        this.node.active = true;
        this.nameLabel.string = this.itemDat.name;
        this.desc.string = this.itemDat.desc;
        this.attrsParent.removeAllChildren();
        for(let k in LanguageAttr){
            if(this.itemDat.attr.hasOwnProperty(k)){
                let node = new cc.Node("attr");                
                let w = node.addComponent(cc.Widget);
                w.alignMode = cc.Widget.AlignMode.ON_WINDOW_RESIZE;
                w.left = w.right = 10;                
                w.isAlignLeft = w.isAlignRight = true  
                
                let label = node.addComponent(cc.Label);
                label.horizontalAlign = cc.Label.HorizontalAlign.LEFT;
                label.overflow = cc.Label.Overflow.CLAMP
                node.height = label.fontSize = label.lineHeight = 20;
                node.parent = this.attrsParent;
                
                let value = this.itemDat.attr[k];
                if(value>0){
                    value = "+"+value;
                }
                label.string = LanguageAttr[k]+":"+value;
                
            }
        }           
    }

    setPoint(world:cc.Vec2){
        let p1 = this.node.parent.convertToNodeSpaceAR(world);        
        let p2 = cc.Camera.main.getWorldToCameraPoint(p1,null);        
        p2.x+=20;
        let limitMinX = -cc.winSize.width/2+10;
        let limitMaxX = cc.winSize.width/2-this.node.width-10;        
                
        if(p2.x>limitMaxX){
            p2.x = p2.x-this.node.width-20
        }

        p2.y-=20;    
        this.node.position = p2;
    }
}
