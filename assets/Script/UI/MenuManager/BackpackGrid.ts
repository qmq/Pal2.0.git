import { ItemDat } from '../../Tools/QType';
import UiItemBasis from './UiItemBasis';
const {ccclass, property} = cc._decorator;

@ccclass
export default class BackpackGrid extends UiItemBasis {
    
    private sprite:cc.Sprite;
    private countLabel:cc.Label;

   
    private count:number;
   

    onLoad(){
        super.onLoad();
        this.sprite = this.node.getChildByName("sprite").getComponent(cc.Sprite);
        this.countLabel = this.node.getChildByName("count").getComponent(cc.Label);
        
    }

    setData(itemDat:ItemDat,count?:number){       
        if(count<=0){
            itemDat = null;
        }
        if(itemDat){
            this.node.name = itemDat.id.toString();
        }else{
            this.node.name = "item";
        }        
        this.count = count; 
        
        
        super.setData(itemDat);
    }

    updateUI(){
        if(this.itemDat){
            this.sprite.spriteFrame = cc.loader.getRes("Item/"+this.itemDat.code,cc.SpriteFrame)
            this.countLabel.string = this.count.toString();    
        }else{
            this.sprite.spriteFrame=null;
            this.countLabel.string = " ";
        }
    }

    useItem(){
        super.useItem();
        /**
         * 判断物品的类型 是否可以使用
         * 装备类，装备物品
         * 使用类，使用
         */
    

        switch(this.itemDat.type){
            case "arms":
            case "cloak":
            case "wear":
            case "hat":
            case "clothes":
            case "shoes":
                //装备物品
                this.onEquip();
            break
        }

    
    }

    /**
     * 换上装备
     */
    onEquip(){
        let e = new cc.Event.EventCustom("OnEquip",true);
        e.setUserData(this.itemDat);
        this.node.dispatchEvent(e);
    }

}
