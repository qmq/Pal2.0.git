import { ItemDat } from '../../Tools/QType';
import Db from '../../Tools/Db';
const {ccclass, property} = cc._decorator;

@ccclass
export default class UiItemBasis extends cc.Component {
    /** 装备ID */
    protected _itemId:number
    set itemId(value){
        if(this.itemId==value)return;
        this._itemId = value;
        if(value>0){
            this.itemDat = Db.Instance.getItem("Item",value);
        }else{
            this.itemDat = null;
        }
        
        this.updateUI();
    }
    get itemId(){
        return this._itemId;
    }    
    protected itemDat:ItemDat;

    /** 点击次数
     * 单击显示物品信息
     * 多击使用物品
     */
    private clickCount:number=0;
    private isClick:boolean=false;


    onLoad(){
        this.node.on(cc.Node.EventType.TOUCH_END,this.touchEnd,this);
    }

    setData(itemDat:ItemDat){
        this.itemDat = itemDat;       
        this.updateUI()
    }

    updateUI(){

    }

    touchEnd(e:cc.Event.EventTouch){        
        this.clickCount++;        
        if(this.isClick)return;
        this.isClick = true;
        this.scheduleOnce(()=>{
            if(this.clickCount==1){
                this.showItemInfo(e)
            }else if(this.clickCount>1){
                this.useItem();
            }
            this.clickCount=0;
            this.isClick=false;
        },0.2)                
    }

    showItemInfo(e:cc.Event.EventTouch){
        let event = new cc.Event.EventCustom("TipPanelShow",true)
        let p = e.getLocation();
        //世界点        
        let p1 =  cc.Camera.main.getCameraToWorldPoint(p,null);
        let dat = {
            itemDat:this.itemDat,
            point:p1
        }
        event.setUserData(dat);        
        this.node.dispatchEvent(event);
    }
    useItem(){
        let event = new cc.Event.EventCustom("TipPanelHide",true);
        this.node.dispatchEvent(event);        
    }    
}