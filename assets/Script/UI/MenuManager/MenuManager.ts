import EquipPanel from './EquipPanel';
import BackpackPanel from './BackpackPanel';
import TipPanel from './TipPanel';
import { ItemDat } from '../../Tools/QType';
import QGame from '../../Tools/QGame';
const {ccclass, property} = cc._decorator;

@ccclass
export default class MenuManager extends cc.Component {
    private equipPanel:EquipPanel;
    private backpackPanel:BackpackPanel
    private TipPanel:TipPanel;
    onLoad(){
        this.equipPanel = this.node.getComponentInChildren(EquipPanel);
        this.backpackPanel = this.node.getComponentInChildren(BackpackPanel);   
        this.TipPanel = this.node.getChildByName("TipPanel").getComponent(TipPanel);
        /**
         * 监听背包中的物品单击，显示物品详细信息
         */
        this.node.on("TipPanelShow",this.showTipPanel,this);
        this.node.on("TipPanelHide",this.hideTipPanel,this);

        /** 更新背包 */
        this.node.on("BackpackUpdate",this.BackpackUpdate,this);
        /** 换装 背包中装备点击事件 */
        this.node.on("OnEquip",this.onEquip,this);

    }

    toggle(name:string){
        let attr = name+"Panel";        
        if(this.hasOwnProperty(attr)){
            this[attr].toggle();
        }else{
            cc.log("菜单"+name+"面板出错")
        }
    }
    showTipPanel(event:cc.Event.EventCustom){
        let dat = event.getUserData();
        let p = dat.point;                                    
        this.TipPanel.itemDat = dat.itemDat;
        this.TipPanel.setPoint(p)
    }
    hideTipPanel(){
        this.TipPanel.node.active = false;
    }
    /**
     * 更新背包
     * 1 卸下装备
     */
    BackpackUpdate(event:cc.Event.EventCustom){
        let itemdat = event.getUserData();        
        this.backpackPanel.updateOnceUI(itemdat)  
    }

    /**
     * 换装
     */
    onEquip(event:cc.Event.EventCustom){
        this.equipPanel.onEquip(event);
    }

}
