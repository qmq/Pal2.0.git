const {ccclass, property} = cc._decorator;

@ccclass
export default class MPanel extends cc.Component {
    protected closeBtn:cc.Node;
    /** 是否已经Start了 */
    protected isRunStart:boolean=false;
    onLoad(){
        this.closeBtn = this.node.getChildByName("close")
        this.closeBtn.on(cc.Node.EventType.TOUCH_END,()=>{
            this.hide();
        })                            
    }
  
    start(){        
        this.isRunStart = true;
    }

    toggle(){
        
        if(this.node.active){
            this.hide();
        }else{            
            this.node.scale = 1;                     
            this.node.active = true;
            this.updateUI();
        }        
    }

    hide(){        
        let arr = [
            cc.scaleTo(0.2,0),
            cc.callFunc(()=>{
                this.node.active = false
            })
        ]
        let seq = cc.sequence(arr);
        this.node.runAction(seq);
    }

    updateUI(){        
       
    }
}
