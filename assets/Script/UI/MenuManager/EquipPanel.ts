import MPanel from './MPanel';
import InfoPanel from './InfoPanel';
import { Team } from '../../Tools/QGame';
import QGame from '../../Tools/QGame';
import Equip from './Equip';
import { ItemDat } from '../../Tools/QType';
const {ccclass, property} = cc._decorator;

@ccclass
export default class EquipPanel extends MPanel {
    private infoToggle:cc.Node;
    private infoPanel:InfoPanel;    
    
    private nameLabel:cc.Label;
    private character:cc.Sprite;
    private arms:Equip;
    private cloak:Equip;
    private wear:Equip;
    private hat:Equip;
    private clothes:Equip;
    private shoes:Equip;
    private team:Team;    
    onLoad(){
        super.onLoad();        
        this.infoToggle = this.node.getChildByName("infoToggle");
        this.infoPanel = this.node.parent.getComponentInChildren(InfoPanel);
        this.infoToggle.on('toggle',this.toggleInfo,this);

        this.nameLabel = cc.find("name",this.node).getComponent(cc.Label);
        this.character = cc.find("character",this.node).getComponent(cc.Sprite);
        this.arms = cc.find("equipL/arms",this.node).getComponent(Equip);
        this.cloak = cc.find("equipL/cloak",this.node).getComponent(Equip);
        this.wear = cc.find("equipL/wear",this.node).getComponent(Equip);
        this.hat = cc.find("equipR/hat",this.node).getComponent(Equip);
        this.clothes = cc.find("equipR/clothes",this.node).getComponent(Equip);
        this.shoes = cc.find("equipR/shoes",this.node).getComponent(Equip);
        this.team = QGame.Instance.Team;
       
        /**
         * 人物选择
         */
        this.character.node.on(cc.Node.EventType.TOUCH_CANCEL,this.CharacterTouchEnd,this);
        this.character.node.on(cc.Node.EventType.TOUCH_END,this.CharacterTouchEnd,this);

        /** 卸下装备 */
        this.node.on("UnloadEquip",this.UnloadEquip,this);

    }
    /** 信息面板的开启与关闭 */
    toggleInfo(toggle:cc.Toggle){                
        this.infoPanel.toggle()
    }
    hide(){
        super.hide();
        if(this.infoPanel.node.active){
            this.infoPanel.hide();
        }        
    }
    /**
     * 更新面板显示数据
     */
    updateUI(){     
        super.updateUI();
        let dat = this.team.getCurrent();        
        this.nameLabel.string = dat.name;
        this.character.spriteFrame = (cc.loader.getRes("Character/"+dat.code,cc.SpriteAtlas) as cc.SpriteAtlas).getSpriteFrame("02");        
        let itemAtlas:cc.SpriteAtlas = cc.loader.getRes("Item/Item",cc.SpriteAtlas);          
        this.arms.itemId = dat.arms;
        this.cloak.itemId = dat.cloak;
        this.wear.itemId = dat.wear;
        this.hat.itemId = dat.hat;
        this.clothes.itemId = dat.clothes;        
        this.shoes.itemId = dat.shoes;        
    }

    //卸下装备
    UnloadEquip(event:cc.Event.EventCustom){
       
        /** 卸下装备的部位名称 */
        let part = event.getUserData();
        /** 卸下装备 */
        let dat = this.team.getCurrent();
        let itemId = dat[part];            
        dat[part]=0;
            
        /** 将装备加入到背包中 */
        let itemdat = QGame.Instance.Backpack.add(itemId);
        
        /** 更新背包 */
        let e = new cc.Event.EventCustom("BackpackUpdate",true);
        e.setUserData(itemdat);
        this.node.dispatchEvent(e);
    }

    /**
     * 换装备||穿上装备
     * @param event 
     */
    onEquip(event:cc.Event.EventCustom){
        
        let dat = this.team.getCurrent();
        let itemDat = event.getUserData() as ItemDat;        
        /** 需要换的装备与原装备不相同 */
        if(dat[itemDat.type]==itemDat.id)return;

        //更换装备
        let itemId = dat[itemDat.type];
        dat[itemDat.type] = itemDat.id;

        //更新装备栏图标
        this[itemDat.type].itemId = itemDat.id;


        //背包中的穿上装备减1
        let itemdat = QGame.Instance.Backpack.sub(itemDat.id)
        let e = new cc.Event.EventCustom("BackpackUpdate",true);
      
        e.setUserData(itemdat);
        this.node.dispatchEvent(e);

        //背包中 卸下的装备加1 如果原先有装备物品
        if(itemId){
            let sitemDat = QGame.Instance.Backpack.add(itemId);
            e.setUserData(sitemDat);
            this.node.dispatchEvent(e);    
        }
    }

    /** 更换显示角色 */
    CharacterTouchEnd(event:cc.Event.EventTouch){       
        let startX = event.getStartLocation().x;
        let endX = event.getLocation().x
        if(startX<endX){                                          
            QGame.Instance.Team.prevShow();
            this.infoPanel.updateUI()
            this.updateUI()                          
        }else if(startX>endX){              
            QGame.Instance.Team.nextShow(); 
            this.infoPanel.updateUI()
            this.updateUI()          
        }
        let e = new cc.Event.EventCustom("TipPanelHide",true);
        this.node.dispatchEvent(e)
    }
}
