import MenuManager from './MenuManager';
const {ccclass, property} = cc._decorator;

@ccclass
export default class MenuBtns extends cc.Component {
    private menuManager:MenuManager;
    onLoad(){
        this.menuManager = this.node.getParent().getComponentInChildren(MenuManager);

        this.initEvents();
    }
    initEvents(){
        this.node.children.forEach(item=>{
            item.on(cc.Node.EventType.TOUCH_END,this.showPanel,this);
        })
    }

    showPanel(event:cc.Event.EventTouch){        
        this.menuManager.toggle(event.target.name);
    }

}
