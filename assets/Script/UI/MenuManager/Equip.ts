import { ItemDat } from '../../Tools/QType';
import Db from '../../Tools/Db';
import UiItemBasis from './UiItemBasis';
import QGame from '../../Tools/QGame';
const {ccclass, property} = cc._decorator;

@ccclass
export default class Equip extends UiItemBasis {
    private sprite:cc.Sprite;
    

    onLoad(){
        super.onLoad();
        this.sprite = this.node.getChildByName("sprite").getComponent(cc.Sprite);
    }

    updateUI(){            
        if(this.itemDat){
            this.sprite.spriteFrame = cc.loader.getRes("Item/"+this.itemDat.code,cc.SpriteFrame);
        }else{
            this.sprite.spriteFrame = null;
        }        
    }

    useItem(){
        super.useItem();
        /** 卸下装备 */          
        if(this.itemId>0)    {
            let event = new cc.Event.EventCustom("UnloadEquip",true);
            event.setUserData( this.node.name );
            this.node.dispatchEvent(event);
            this.itemId = 0;        
        }
    }
  
}
