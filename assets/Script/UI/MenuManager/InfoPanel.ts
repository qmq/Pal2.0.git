import MPanel from './MPanel';
import QGame from '../../Tools/QGame';
import { Team } from '../../Tools/QGame';
const {ccclass, property} = cc._decorator;

@ccclass
export default class InfoPanel extends MPanel {
    private lv:cc.Label;
    private physical:cc.Label;
    private magic:cc.Label;
    private defense:cc.Label;
    private bodylaw:cc.Label;
    private luck:cc.Label;
    private team:Team;

    onLoad(){
        super.onLoad();
        this.lv = cc.find("Content/Lv/value",this.node).getComponent(cc.Label);
        this.physical = cc.find("Content/Physical/value",this.node).getComponent(cc.Label);
        this.magic = cc.find("Content/Magic/value",this.node).getComponent(cc.Label);
        this.defense = cc.find("Content/Defense/value",this.node).getComponent(cc.Label);
        this.bodylaw = cc.find("Content/Bodylaw/value",this.node).getComponent(cc.Label);
        this.luck = cc.find("Content/Luck/value",this.node).getComponent(cc.Label);                
        this.team = QGame.Instance.Team;              
    }

    

    updateUI(){
        super.updateUI();
        if(!this.node.active)return;
        
        let dat = QGame.Instance.Team.getCurrent()
        this.lv.string = dat.lv.toString();
        this.physical.string = dat.physical.toString();
        this.magic.string = dat.magic.toString();
        this.defense.string = dat.defense.toString();
        this.bodylaw.string = dat.bodylaw.toString();
        this.luck.string = dat.luck.toString();        
    }
}
