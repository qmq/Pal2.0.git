import ShopPanel from './Shop/ShopPanel';
import DialogPanel from './Dialog/DialogPanel';
import { DialogDat } from '../Tools/QType';
const {ccclass, property} = cc._decorator;

@ccclass
export default class UIManager extends cc.Component {
    private widget:cc.Widget;
    private ShopPanel:ShopPanel;
    private DialogPanel:DialogPanel;

    onLoad(){
        this.node.position = cc.Vec2.ZERO
        this.widget = this.getComponent(cc.Widget);
        this.widget.enabled = true;
        
        this.ShopPanel = this.node.getChildByName("ShopPanel").getComponent(ShopPanel);
        this.DialogPanel = this.node.getChildByName("DialogPanel").getComponent(DialogPanel);
     
    }

    start(){
        //this.openShop();
    }


    /**
     * 打开商店
     */
    openShop(items:number[]){      
        this.ShopPanel.openShop(items);
    }

    /**
     * 打开对话框
     */
    openDialog(dat:DialogDat){        
        this.DialogPanel.show(dat);
    }
}
