import BCharacter from './Basis/BCharacter';
import { CharacterType, EnemyDat } from '../Tools/QType';
const {ccclass, property} = cc._decorator;

@ccclass
export default class BattleManager extends cc.Component {
    private dats=[
        {
            "code":"成年王小虎",
            "name":"王小虎",
            "lv":1,
            "type":0,
            "hp":100,
            "mp":50,
            "maxHp":100,
            "maxMp":50,
            "physical":10,
            "defense":10,
            "magic":10,
            "bodylaw":10,
            "luck":10
        },
        {
            "code":"铜鞭男",
            "name":"铜鞭男",
            "lv":1,
            "type":2,
            "hp":100,
            "mp":50,
            "maxHp":100,
            "maxMp":50,
            "physical":10,
            "defense":10,
            "magic":10,
            "bodylaw":10,
            "luck":10
        },
        {
            "code":"铜鞭男",
            "name":"铜鞭男",
            "lv":1,
            "type":2,
            "hp":100,
            "mp":50,
            "maxHp":100,
            "maxMp":50,
            "physical":10,
            "defense":10,
            "magic":10,
            "bodylaw":10,
            "luck":10
        }        
    ]

    /**
     * 当前存活的玩家
     */
    private players:BCharacter[] = [];
    /**
     * 当前存活的敌人
     */
    private enemys:BCharacter[] = [];
    
    /** 当前执行攻击方 */
    private roundType:CharacterType;
    /** 胜利方 */
    private winType:CharacterType;

    /** 回合进行中 */
    private isRound:boolean=true;

    onLoad(){
        /** 监听角色控制结束事件 */
        this.node.on("ControlEnd",this.round,this);
        /** 监听角色死亡事件 */
        this.node.on("Die",this.die,this);
    }

    start () {  
        this.roundType = CharacterType.Player;
        
        this.init();        
    }
    /**
     * 初始化
     */
    init(){
        //需要加载的资源
        let dats = this.dats;

        let resArray = dats.map((item)=>{
            return "Prefab/Battle/"+item.code
        })

        cc.loader.loadResArray(resArray,(err,res)=>{
            this.createCharacter();
        })
    }

    /**
     * 创建角色
     */
    createCharacter(){
        let dats = this.dats;
        dats.forEach((item)=>{
            let node = cc.instantiate<cc.Node>(cc.loader.getRes("Prefab/Battle/"+item.code,cc.Prefab))
            let bc = node.getComponent(BCharacter)
            bc.setDat(item);
            node.setParent(this.node);
            if(item.type==CharacterType.Player){
                this.players.push(bc)
            }else if(item.type==CharacterType.Enemy){
                this.enemys.push(bc)
            }
        })
        
        this.round();
    }

    /** 回合控制 */
    round(){        
        if(!this.isRound){
            return false;
        }
        

        let currentTarget:BCharacter;
        let target:BCharacter;
        let index;
        switch(this.roundType){
            case CharacterType.Player:
                //查找玩家一方可操作的角色
                index = this.players.findIndex(item=>{
                    return item.getReady();
                })
                if(index==-1){
                    //所有角色都执行完操作 更换攻击方
                    this.roundCurrentType();                    
                    return;
                }else{
                    currentTarget = this.players[index];         
                    let r = Math.floor(Math.random()*this.enemys.length);
                    target = this.enemys[r]
                }
            break;
            case CharacterType.Enemy:
                index = this.enemys.findIndex(item=>{
                    return item.getReady();
                })
                if(index==-1){
                    this.roundCurrentType();
                    return;
                }else{
                    currentTarget = this.enemys[index];
                    let r = Math.floor(Math.random()*this.players.length);
                    target = this.players[r]
                }
            break;
        }

        if(currentTarget){            
            currentTarget.attack(target);
        }

    }

    /**
     *  检测战斗是否已结束 
     *  返回值 true 战斗结束
     *  返回值 false 战斗未结束
    */
    checkBattleEnd(){
        if(this.players.length===0){
            this.winType = CharacterType.Enemy;
            return true;
        }
        if(this.enemys.length===0){
            this.winType = CharacterType.Player;
            return true;
        }
        return false;
    }

    /**
     * 转换当前操作的阵营
     */
    roundCurrentType(){        
        switch(this.roundType.toString()){
            case CharacterType.Player.toString():
                //将敌方的准备状态重置
                this.enemys.forEach(item=>{
                    item.resetReady();
                })
                this.roundType = CharacterType.Enemy;                
            break
            case CharacterType.Enemy.toString():
                //将我方的准备状态重置
                this.players.forEach(item=>{
                    item.resetReady()
                })
                this.roundType = CharacterType.Player;                
            break
        }              
        this.round();
    }

    /** 角色死亡 */
    die(event:cc.Event.EventCustom){
        let bc = event.getUserData() as BCharacter;
        let index;
        switch(bc.getCamp()){
            case CharacterType.Enemy:
                index = this.enemys.indexOf(bc);
                if(index>-1){
                    this.enemys.splice(index,1)
                }
            break;
            case CharacterType.Player:
                index = this.players.indexOf(bc);                
                if(index>-1){
                    this.players.splice(index,1)
                }
            break;
        }
        if(this.checkBattleEnd()){
            this.isRound = false;
            cc.log(`战斗结束${this.winType}胜利`)
        }
    }
}
