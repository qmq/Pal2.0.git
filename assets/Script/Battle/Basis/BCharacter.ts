import BPlayer from '../BPlayer';
import BEnemy from '../BEnemy';
import { ButtleDat } from '../../Tools/QType';

const {ccclass, property} = cc._decorator;

@ccclass
export default class BCharacter extends cc.Component {  
    protected _animation:cc.Animation;
    get animation(){
        if(!this._animation){
            this._animation = this.getComponent(cc.Animation);
        }
        return this._animation;
    }

    protected _hpBar:cc.ProgressBar;
    get hpBar(){
        if(!this._hpBar){
            this._hpBar = this.node.getChildByName("hp").getComponent(cc.ProgressBar);
            this._hpBar.node.y = this.node.height/2+10
        }
        return this._hpBar;
    }

    /** 攻击对象 */
    protected target:BCharacter;

    /** 移动前的点位置 */
    protected startPoint:cc.Vec2;

    protected battleDat:ButtleDat;

    /**
     * 是否存活 false即角色死亡
     */
    protected isLive:boolean=true;

    /**
     * 是否准备好 false表达本回合已结束控制
     */
    protected isReady:boolean=true;


    setDat(dat){
        this.battleDat = dat;
        this.showHpBar();
    }

    /** 
     * 动画分析 
     * 1 正常状态
     * 2 攻击
     * 3 二连击
     * 4 蓄力攻击
    */
    start () {        
        this.startPoint = this.node.position;     
    }

    /** 显示当前血量 */
    showHpBar(){
        cc.log( this.battleDat.name+" "+this.battleDat.hp )
        this.hpBar.progress = this.battleDat.hp/this.battleDat.maxHp;
    }

    /**
     * 攻击
     */
    attack(target:BCharacter){

    }

    /** 操作结束 通知管理器 */
    ControlEnd(){
        this.isReady = false;
        let event = new cc.Event.EventCustom("ControlEnd",true);
        this.node.dispatchEvent(event);
    }
    /**
     * 受到普通攻击
     */
    beGiven(){
        let className = this.constructor.name;
        let dir = 1;
        if(className=="BPlayer"){
            dir = -1;
        }
        
        


        let a1 = cc.blink(0.8,5);
        let a2 = cc.moveBy(0.4,dir*-30,0);
        let a3 = cc.moveBy(0.4,dir*30,0);

        let seq = cc.sequence(a2,a3);
        let spawn = cc.spawn(a1,seq);
        this.node.runAction(spawn);

        this.battleDat.hp-=50;
        if(this.battleDat.hp<=0){
            this.die()
        }
        this.showHpBar();
    }

    getLive(){
        return this.isLive;
    }
    getReady(){
        return this.isReady;
    }
    /** 获取阵营 */
    getCamp(){
        return this.battleDat.type;
    }
    resetReady(){
        this.isReady = true;
    }

    /** 角色死亡 */
    die(){
        this.battleDat.hp=0;
        this.isLive = false;
        let event = new cc.Event.EventCustom("Die",true);
        event.setUserData(this);
        this.node.dispatchEvent(event);
    }

}
