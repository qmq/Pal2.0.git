import BCharacter from './Basis/BCharacter';
const {ccclass, property} = cc._decorator;

@ccclass
export default class BEnemy extends BCharacter {


    onLoad () {}

    

    attack(target:BCharacter){
        this.target = target;
        this.general();
    }


    /** 普通攻击 */
    general(){

        
        this.animation.play("2");
    }

    /** 攻击动画 */
    AttackAnim(action:string){              
        let cilp = this.animation.currentClip;
        let animState = this.animation.getAnimationState(cilp.name);
        animState.pause();
        switch(action){
            case "move":                                     
                let p1 = this.target.node.position.add( cc.v2(-60,-10) );
                let a11 = cc.jumpTo(1,p1,100,1);
                let a12 = cc.callFunc(()=>{                                
                    animState.play()
                })                
                let seq1 = cc.sequence(a11,a12);
                this.node.runAction(seq1);
            break;
            case "shock":                
                let a21 = cc.callFunc(()=>{
                    this.target.beGiven();
                })
                let a22 = cc.delayTime(0.8);                
                let a23 = cc.callFunc(()=>{
                    animState.play()
                })
                let seq2 = cc.sequence(a21,a22,a23);
                this.node.runAction(seq2);
            break;
            case "reset":                
                let a31 = cc.jumpTo(1,this.startPoint,50,1);
                let a32 = cc.callFunc(()=>{
                    animState.play()
                })
                var seq3 = cc.sequence(a31,a32);
                this.node.runAction(seq3);
            break;
            case "end":                
                this.animation.play("1");
                this.ControlEnd()
            break;
        }        
    }
}
