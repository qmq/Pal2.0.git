import { PlayerDat, ItemDat, CharacterDir } from './QType';
import Db from './Db';

/**
 * 队伍管理类
 */
export class Team{
    private _characters:PlayerDat[] = [];    
    /** 当前显示的角色Index */
    private _current:number=0;
    get length(){
        return this._characters.length;
    }
    /** 添加一个队员 */
    add(dat:PlayerDat){    
        //如果加入的角色不存在方向与位置，自动定位        
        if(dat.dir==undefined){
            let last =this._characters[this.length-1] 
            
            dat.dir = last.dir;
            if(dat.dir==CharacterDir.Up||dat.dir==CharacterDir.Down){
                dat.x = last.x;
                if(dat.dir==CharacterDir.Up){
                    dat.y = last.y-40;    
                }else{
                    dat.y = last.y+40;    
                }                
            }else if(dat.dir==CharacterDir.Left||dat.dir==CharacterDir.Right){
                dat.y=last.y;
                if(dat.dir==CharacterDir.Right){
                    dat.x = last.x-40
                }else{
                    dat.x = last.x+40
                }                
            }
        }        
        this._characters.push(dat);
    }
    /** 移除一个队员 */
    remove(name:string){    
        let index = this._characters.findIndex(item=>item.name==name);
        if(index>-1){
            this._characters.splice(index,1)
        }else{
            cc.log("移除队员人不存在")
        }
    }
    /** 查找一个队员 */
    find(name:string){
        let index = this._characters.findIndex(item=>item.name==name)
        if(index>-1){
            return this._characters[index]
        }else{
            cc.log("无法查找到队员");
        }
    }
    getAt(index:number){
        return this._characters[index];
    }
    /**
     * 通过名称查找队员的Index
     */
    getIndexByName(name:string){
        let index = this._characters.findIndex(item=>{
            return item.name==name
        })
        return index;
    }
    /**
     * 获取当前在面板中要显示的角色
     */
    getCurrent(){
        return this._characters[this._current];
    }
    /**
     * 获取下一个在面板中要显示的角色
     */
    nextShow(){
        this._current++;
        if(this._current>=this.length){
            this._current=0;
        }
        return this.getCurrent()
    }
    /**
     * 获取上一个在面板中要显示的角色
     */
    prevShow(){
        this._current--;
        if(this._current<0){
            this._current = this.length-1;
        }
        return this.getCurrent()
    }

}

/**
 * 背包管理类
 */
export class Backpack{
    private _items:{ItemDat:ItemDat,count:number}[] = [];
    get length(){
        return this._items.length;
    }

    /**初始化背包 */
    init(dat:{id:number,count:number}[]){
        this._items = [];
        if(!dat || !dat.length)return;
        dat.forEach(item=>{
            if(item.count){
                let idat = Db.Instance.getItem<ItemDat>("Item",item.id)
                this._items.push({
                    ItemDat:idat,
                    count:item.count
                });    
            }
        })
    }

    getAt(index:number){
        return this._items[index];
    }
    /**
     * 添加物品
     * 返回物品的信息及数量
     * @param itemId 物品ID
     * @param count  物品数量
     */
    add(itemId:number,count:number=1):{ItemDat:ItemDat,count:number}{
        if(count<=0)return;    
        let index = this._items.findIndex(item=>{
             return item.ItemDat.id==itemId
        })
        
        if(index>-1){
            //物品已存在,修改物品数量
            this._items[index].count+=count
            return this._items[index];
        }else{
            //物品不存在，背包中新加入物品
            let dat = Db.Instance.getItem<ItemDat>("Item",itemId);
            let obj = {
                ItemDat:dat,
                count:count
            }
            this._items.push(obj)
            return obj
        }
    }
    /**
     * 减少物品
     * @param ItemId 物品ID
     * @param count 数量
     */
    sub(itemId:number,count:number=1){
        if(count<=0)return;    
        let index = this._items.findIndex(item=>{
             return item.ItemDat.id==itemId
        })
        
        if(index>-1){
            //物品已存在,修改物品数量
            this._items[index].count-=count
            if(this._items[index].count<=0){
                return this._items.splice(index,1)[0];
            }                        
            return this._items[index];
        }
    }
}


export interface MapState{[name:string]:{
    /** 
     * 独立参数
     * 每个地图独立的开关
     */
    [key:string]:any
    /**
     * 进入地图的次数
     * 初次进入地图时，可以触发一些事件
     */
    enter:number
}}

/**
 * 地图管理
 * 1 玩家当前所在的地图
 * 2 玩家在地图中的状态(对事件的影响)
 */
export class Map{
    name:string
    private _state:MapState={}
    /**
     * 获取当前场景的状态
     */
    getState(name?:string){   
        let key = name||this.name;
        if(!this._state.hasOwnProperty(key)){
            this._state[key] = {
                enter:0
            }
        }
        return this._state[key];
    }
}


export default class QGame{
    public static Instance:QGame=new QGame;
    private _team:Team;
    private _map:Map;
    /** 金币 */
    private _money:number=0;
    get money(){
        return this._money;
    }    
    get Team(){
        return this._team;
    }
    private _backpack:Backpack;
    get Backpack(){
        return this._backpack;
    }
    get Map(){
        return this._map
    }

    constructor(){                
        this._team = new Team();
        this._backpack = new Backpack();
        this._map = new Map();
    }        

    /** 变更金额 
     * value>0 加
     * value<0 减
    */
    changeMoney(value:number){
        let v = this._money+value;
        if(v<0){
            v=0;
        }
        this._money = v;
    }
}

