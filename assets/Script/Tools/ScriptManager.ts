import MapTerrain from '../Game/MapTerrain';
import DialogPanel from '../UI/Dialog/DialogPanel';
import Rocket from '../UI/Rocker';

/** 静态脚本管理器 */
export default class ScriptManager{
    static readonly Instance:ScriptManager=new ScriptManager();
    /** 地图地型 */
    public MapTerrain:MapTerrain;
    /** UI 对话框 */
    public DialogPanel:DialogPanel;
    /** 摇杆 */
    public Rocket:Rocket;
}