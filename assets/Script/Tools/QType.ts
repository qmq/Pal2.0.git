export let LanguageAttr={
    physical:"武术",
    defense:"防御",
    magic:"灵力",
    bodylaw:"身法",
    luck:"吉运",
    fire:"火属性",
    roundHp:"血量/回合",
    roundMp:"真气/回合",
    useMagicSubMp:"法术减少"
}

/**
 * 地图数据
 * 1 Npc
 * 2 剧情
 */
export interface MapDat{
    npc?:NpcDat[]
    plot?:any
}

/**
 * 物品数据
 */
export interface ItemDat{
    /** ID */
    id:number
    /** 名称 */
    name:string
    /** 图片 */
    code:string   
    /** 类型 */
    type:string 
    /** 描述 */
    desc:string    
    /** 单价 */
    price:number
    /** 属性 */
    attr:{
        physical?:number
        defense?:number
        magic?:number 
        bodylaw?:number
        luck?:number
    }
    /** 使用限制 */
    stint?:{
        player:string
    }
}


/**
 * 地图上 角色状态
 */
export enum CharacterState{
    /** 空闲 */
    Idle,
    /** 移动 */
    Move,
    /** 自动移动 */
    AutoMove
}

/**
 * 地图上 角色方向
 */
export enum CharacterDir{
    /** 上 */
    Up,
    /** 下 */
    Down,
    /** 左 */
    Left,
    /** 右 */
    Right
}

/** 角色类型 */
export enum CharacterType{
    /** 玩家及队友 */
    Player,
    /** Npc */
    Npc,
    /** 敌人 */
    Enemy
}
/**
 * 性别 
 */
export enum Sex{
    无,
    男,
    女
}

/**
 * 角色数据
 */
export interface CharacterDat{
    /** 素材名称 */
    code:string
    /** 人物名称 */
    name:string
    /** 类型 */
    type:CharacterType
    /**
     * 性别
     */
    sex:Sex,
    /**
     * X坐标
     */
    x:number
    /**
     * Y坐标
     */
    y:number
    /**　方向 */
    dir:CharacterDir
    /** 状态 */
    state:CharacterState
    _node?:cc.Node    
}

/**
 * 战斗角色数据
 */
export interface ButtleDat extends CharacterDat{
    /** 等级 */
    lv:number
    /** 血量 */
    hp:number
    /** 真气 */
    mp:number
    /** 最大血量 */
    maxHp:number
    /** 最大法力 */
    maxMp:number
    /** 武术 */
    physical:number
    /** 防御 */
    defense:number
    /** 法术攻击 */
    magic:number
    /** 身法 */
    bodylaw:number
    /** 吉运 */
    luck:number
    /** 武器 */
    arms:number
    /** 披风 */
    cloak:number
    /** 腰佩 */
    wear:number
    /** 帽子 */
    hat:number
    /** 衣服 */    
    clothes:number
    /** 鞋子 */
    shoes:number
}

/**
 * Npc数据
 */
export interface NpcDat extends CharacterDat{	          
    speak?:{
        /** 默认说话 */
        normal:PlotDat[],        
        /** 开关说话 某个开关打开时，说某些话 */
        if?:{[key:string]:DialogDat}
    }    
    /** 是否是商人 */
    trader:boolean
    /** 货物 */
    goods?:number[]    
}



/**
 * 玩家数据
 */
export interface PlayerDat extends ButtleDat{
    
}

/**
 * 敌人数据
 */
export interface EnemyDat extends ButtleDat{
    
}


/**
 * 对话数据类型
 */
export enum DialogType{
    /** 交流 */
    Chat,
    /** 地图名称 */
    City
}


/**
 * UI中对话数据
 * 
 */
export interface DialogDat{
    type:DialogType
    /** 说话人的名称 */
    name?:string
    /** 内容 */
    content:string
    afterEvent?:{
        event:string,
        obj:{
            name?:string,
            data?:any
        }
    }
}


/**
 * 剧情类型
 */
export enum PlotType{    
    /** 角色移动 */
    Move,
    /** 对话 */
    Dialog
}

/**
 * 剧情角色移动
 */
export interface PlotCharacterMove{
    /** 角色名称 */
    name:string
    /** 移动到某点 */
    to:cc.Vec2
}

/**
 * 剧情数据
 */
export interface PlotDat{
    /** 类型 */
    type:PlotType
    /** 数据 */
    data:DialogDat|PlotCharacterMove
}