import { ButtleDat, ItemDat } from './QType';
export default class Db{
    public static readonly Instance:Db=new Db;    
    private Character:{[key:string]:ButtleDat} = {};
    private Item:{[key:string]:ItemDat} = {};

    public setData(key,value){                
        this[key] = value
    }
    public getItem<T>(key,name):T{
        return this[key][name];
    }

}